let info=
[{ 
    name: 'ashi', age: '18', id: '501' 
}, 
{ 
    name: 'ashika', age: '19', id: '502' 
}, 
{ 
    name: 'priya', age: '20', id: '503' 
}, 
{ 
    name: 'ashika', age: '21', id: '504' 
}, 
{ 
    name: 'priya', age: '22', id: '505' 
}],
    result=info.reduce(groupby=(group, obj)=> {
        group[obj.name]=group[obj.name] || [];
        group[obj.name].push(obj);
        return group;
    }, Object.create(null));
console.log(result);